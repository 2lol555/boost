﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	Rigidbody myRigidbody;
	AudioSource myAudio;

	[SerializeField] float MainTrhust;
	[SerializeField] float rcsForce;
	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody>();
		myAudio = GetComponent<AudioSource>();
		MainTrhust = MainTrhust * 10;
	}
	
	// Update is called once per frame
	void Update () {
		ProcessInput ();
	}
	private void ProcessInput (){
		Thrust ();
		RcsControl ();
	}

	void Thrust (){
		if (Input.GetKey (KeyCode.Space)) {
			myRigidbody.AddRelativeForce (new Vector3 (0f, MainTrhust, 0f));
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			myAudio.Play ();
		}else if (Input.GetKeyUp (KeyCode.Space)) {
			myAudio.Stop ();
		}
	}

	void RcsControl (){
		
		if (Input.GetKey (KeyCode.A)) {
			myRigidbody.freezeRotation = true;
			transform.Rotate (Vector3.forward * Time.deltaTime * rcsForce * 10);
		}else if (Input.GetKey (KeyCode.D)) {
			transform.Rotate (Vector3.back * Time.deltaTime * rcsForce * 10);
			myRigidbody.freezeRotation = true;
		}else{
			myRigidbody.freezeRotation = false;	
		}

	}
	void OnCollisionEnter(Collision other){
//		if(other.collider.tag != "Friendly"){
//			Debug.Log("Dead");
//		}else{
//			Debug.Log("Ok");
//		}
		switch(other.gameObject.tag){
			case "Friendly":
				Debug.Log("Ok"); //TODO remove
				break;
			case "Fuel":
				Debug.Log("Fuel Point touched");
				break;
			case "Finish":
				Debug.Log("Finished");
				break;
			default:
				Debug.Log("Dead");
				break;
			
		}
	}
}
